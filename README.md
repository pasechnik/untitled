# Untitled automation script

## Startup

1. Start mongoDB with docker-compose
   ```shell script
   docker-compose up
   ```
2. open local [Mongo Express](http://localhost:8081/)

3. Create Database **untitled** in MongoDB

4. Update your .env file with your values

   | N   | Variable                   | Value                                                            |
   | --- | -------------------------- | ---------------------------------------------------------------- |
   | 1   | MONGO_DB_URL               | mongodb://root:example@localhost:27017/untitled?authSource=admin |
   | 2   | MONGO_DB_NAMESPACE         | untitled                                                         |
   | 3   | MONGO_COLLECTIONS_PATIENTS | Patients                                                         |
   | 4   | MONGO_COLLECTIONS_EMAILS   | Emails                                                           |

5. install dependencies

   ```shell script
   yarn
   ```

6. run validate
   ```shell script
   node ./index.js validate -f data.csv
   ```
7. run import
   ```shell script
   node ./index.js import -f data.csv
   ```
8. run validate

   ```shell script
   node ./index.js validate -f data.csv
   ```

9. For automated scenario log level can be decreased to the Error level

   ```shell script
    DEBUG=script:Error
   ```

10. Docker image can be built and used with different env variables

## Tasks completed

- [x] get input parameter with file name
- [x] check file existence
- [x] get database connection string
- [x] connect to a MongoDB
- [x] parse csv file
- [x] validate data
- [x] create data in db
- [x] print report
