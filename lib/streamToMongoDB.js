'use strict';
// source: stream-to-mongo-db npm

var _stream = require('stream');

var _mongodb = require('mongodb');

module.exports = {
  streamToMongoDB: (options) => {
    const config = Object.assign(
      // default config
      {
        batchSize: 1,
        insertOptions: { w: 1 },
        dbConnection: undefined,
      },
      // override options
      options
    );

    // those variables can't be initialized without Promises, so we wait first drain
    let client;
    let dbConnection;
    let collection;
    let records = [];

    // this function is useful to insert records and reset the records array
    const insert = async () => {
      await collection.insertMany(records, config.insertOptions);
      records = [];
    };

    const close = async () => {
      if (!config.dbConnection && client) {
        await client.close();
      }
    };

    const checkCollection = async () => {
      if (!dbConnection) {
        if (config.dbConnection) {
          dbConnection = config.dbConnection; // eslint-disable-line prefer-destructuring
        } else {
          client = await _mongodb.MongoClient.connect(config.dbURL, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
          });
          dbConnection = await client.db();
        }
      }
      if (!collection)
        collection = await dbConnection.collection(config.collection);
    };

    // stream
    const writable = new _stream.Writable({
      objectMode: true,
      write: async (record, encoding, next) => {
        try {
          // connection
          await checkCollection();

          // add to batch records
          records.push(record);

          // insert and reset batch recors
          if (records.length >= config.batchSize) await insert();

          // next stream
          next();
        } catch (error) {
          await close();
          writable.emit('error', error);
        }
      },
    });

    writable.on('finish', async () => {
      try {
        if (records.length > 0) await insert();
        await close();

        writable.emit('close');
      } catch (error) {
        await close();

        writable.emit('error', error);
      }
    });

    writable.clearAll = async () => {
      await checkCollection();
      await collection.deleteMany({});
    };
    writable.close = async () => {
      await close();
    };

    return writable;
  },
};
