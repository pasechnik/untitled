const { v4: uuid } = require('uuid');
// import debug from 'debug';
const { getCollection, close, decrease } = require('./db-mongo');

// const error = debug('app:engine:error');
// const log = debug('app:engine');
// log.log = console.log.bind(console);

class Engine {
  constructor(collectionName, namespace, key) {
    this.namespace =
      namespace === undefined || !namespace.length ? 'untitled' : namespace;
    this.collectionName =
      collectionName === undefined || !collectionName.length
        ? uuid()
        : collectionName;
    this.key = key || 'name';
    this.collection = null;
  }

  getCollection() {
    if (!this.collection) {
      this.collection = getCollection(this.collectionName, this.namespace);
    }
    return this.collection;
  }

  setCollectionName(name) {
    if (name !== undefined) this.collectionName = name;
    if (this.collection) {
      this.collection = getCollection(this.collectionName, this.namespace);
      decrease();
    }
    return this;
  }

  setKeyName(key) {
    if (key !== undefined) this.key = key;
    return this;
  }

  async count(q) {
    try {
      const query =
        q !== undefined && q.length ? { [this.key]: new RegExp(q) } : {};
      const collection = await this.getCollection();
      return collection.countDocuments(query);
    } catch (e) {
      console.error(e);
      return 0;
    }
  }

  async has(key) {
    try {
      const collection = await this.getCollection();
      const r = await collection.findOne({ [this.key]: key });
      return r != null;
      // log(r)
    } catch (e) {
      throw e;
    }
  }

  async get(key, def = null) {
    try {
      const collection = await this.getCollection();
      const r = await collection.findOne({ [this.key]: key });
      return r == null ? def : r;
    } catch (e) {
      throw e;
    }
  }

  async set(key, data) {
    try {
      const collection = await this.getCollection();
      const r = await collection.findOneAndReplace(
        { [this.key]: key },
        { ...data, [this.key]: key },
        { upsert: true, returnNewDocument: true }
      );

      if (r.value == null) {
        r.value = await this.get(key);
      }

      return {
        numAffected: r.lastErrorObject.n,
        affectedDocuments: r.value,
        upsert: r.lastErrorObject.updatedExisting,
      };
    } catch (e) {
      throw e;
    }
  }

  async getAll(q) {
    try {
      const query =
        q !== undefined && q.length ? { [this.key]: new RegExp(q) } : {};
      const collection = await this.getCollection();
      const r = await collection.find(query);
      const items = await r.toArray();
      return items;
      // log(items)
    } catch (e) {
      throw e;
    }
  }

  async findAll(q, sort) {
    try {
      const query = q !== undefined ? q : {};
      const collection = await this.getCollection();
      let r = collection.find(query);
      if (sort !== undefined) {
        r = r.sort(sort);
      }
      const items = await r.toArray();
      return items;
      // log(items)
    } catch (e) {
      throw e;
    }
  }

  async clear() {
    try {
      const collection = await this.getCollection();
      const { result } = await collection.deleteMany({});
      return result.n;
      // log(result)
    } catch (e) {
      throw e;
    }
  }

  async delete(key) {
    try {
      const collection = await this.getCollection();
      const { result } = await collection.remove({ [this.key]: key });
      return result.n;
      // log(result)
    } catch (e) {
      throw e;
    }
  }

  async close(force = false) {
    try {
      await close(force);
    } catch (e) {
      throw e;
    }
  }
}

module.exports = Engine;
