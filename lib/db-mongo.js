const mongo = require('mongodb').MongoClient;

// environment variables
const MONGO_DB_URL =
  process.env.MONGO_DB_URL ||
  'mongodb://root:example@localhost:27017/untitled?authSource=admin';

let _connection = null;

const connect = (url) =>
  mongo.connect(url, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
  });

let connections = 0;
/**
 * Returns a promise of a `db` object. Subsequent calls to this function returns
 * the **same** promise, so it can be called any number of times without setting
 * up a new connection every time.
 */
const connection = (url) => {
  connections++;
  if (_connection === null) {
    _connection = connect(url);
  }

  return _connection;
};

/**
 * Returns a ready-to-use `collection` object from MongoDB.
 *
 * Usage:
 *
 *   (await getCollection('users')).find().toArray().then( ... )
 */

const getCollection = async (collection, namespace) => {
  const client = await connection(MONGO_DB_URL);
  return client.db(namespace).collection(collection);
};

const decrease = function () {
  connections--;
};

const close = async function (force = false) {
  connections--;

  try {
    if (_connection && connections < 1) {
      const client = await _connection;
      await client.close();
      _connection = null;
    }
  } catch (e) {
    throw e;
  }
};

module.exports = {
  close,
  getCollection,
  connection,
  decrease,
};
