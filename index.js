// load environment from .env file
if (process.env.NODE_ENV !== 'production') {
  require('dotenv').config();
}

// import absolute libraries
const { v4: uuid } = require('uuid');
const debugBase = require('debug')('script');
const { program } = require('commander');
const fs = require('fs');
const csv = require('csv-stream');
const equal = require('deep-equal');
const diff = require('rus-diff').diff;
const addDays = require('date-fns/addDays');
// import local libraries
const Engine = require('./lib/engine-mongo');
const streamToMongoDB = require('./lib/streamToMongoDB').streamToMongoDB;
// absolute version
// const streamToMongoDB = require('stream-to-mongo-db').streamToMongoDB;

// logging functions
const debug = debugBase.extend('log');
const debugDB = debugBase.extend('db');
const debugValid = debugBase.extend('valid');
const debugError = debugBase.extend('Error');

// env variables
const dbURL =
  process.env.MONGO_DB_URL ||
  'mongodb://root:example@localhost:27017/untitled?authSource=admin';

const mongoDB = process.env.MONGO_DB_NAMESPACE || 'untitled';
const patientsCollection = process.env.MONGO_COLLECTIONS_PATIENTS || 'Patients';
const emailsCollection = process.env.MONGO_COLLECTIONS_EMAILS || 'Emails';

// csv format options
const csvOptions = {
  delimiter: '|', // default is ,
};

/**
 * Checks flat file existence
 *
 * @param path filename to check
 * @returns {Promise<void>}
 */
async function checkFileExistence(path) {
  await fs.promises.access(path);
  const stat = await fs.promises.lstat(path);
  if (!stat.isFile()) {
    throw Error(`${path} is not a file`);
  }
  debug('file exists');
}

/**
 * Clears data for collections
 *
 * @param collection names args as array
 * @returns {Promise<void>}
 */
async function clearCollections(...collections) {
  const engine = new Engine('test', mongoDB);

  await Promise.all(
    collections.map((name) => {
      debugDB('clearing collection "%s"', name);
      return engine.setCollectionName(name).clear();
    })
  );

  await engine.close();
}

/**
 * imports data from csv file
 *
 * @param path filename for csv file
 * @returns {Promise<void>}
 */
async function csvFile2Mongo(path) {
  try {
    debug('clearing collections');
    await clearCollections(patientsCollection, emailsCollection);

    debug('creating db writer streams');
    const patientsToMongo = streamToMongoDB({
      dbURL,
      collection: patientsCollection,
    });
    const emailsToMongo = streamToMongoDB({
      dbURL,
      collection: emailsCollection,
    });

    debug('creating file stream');
    fs.createReadStream(path)
      .pipe(csv.createStream(csvOptions))
      .on('data', (chunk) => {
        debug('writing Patient %O', chunk['Member ID'] || '<empty>');
        debugDB('writing Patient %O', chunk);
        patientsToMongo.write(chunk);
        if (chunk.CONSENT === 'Y') {
          debug('  creating emails');
          const today = new Date();
          for (let i = 1; i < 5; i++) {
            debug('    %O', `Day ${i}`);
            const email = {
              emailID: uuid(),
              patient: chunk,
              subject: `Day ${i}`,
              sendDate: addDays(today, i - 1),
              sent: false,
            };
            debugDB('writing Email %O', email);
            emailsToMongo.write(email);
          }
        }
      })
      .on('end', async () => {
        debug('closing DB streams');
        patientsToMongo.end();
        emailsToMongo.end();
        debug('done!');
      });
  } catch (e) {
    debugError(e);
  }
}

/**
 * Validates imported and csv data
 *
 * @param path
 * @returns {Promise<void>}
 */
async function csvFileValidate(path) {
  try {
    debug('creating file stream');
    fs.createReadStream(path)
      .pipe(csv.createStream(csvOptions))
      .on('data', async (chunk) => {
        try {
          const memberId = chunk['Member ID'] || '<empty>';
          let valid = true;

          const patients = new Engine(patientsCollection, mongoDB, 'Member ID');
          const { _id, ...patient } = await patients.get(memberId, {
            _id: undefined,
          });
          await patients.close();

          // 1. Verify the data in flat file matches the data in patient collection
          if (_id === undefined) {
            valid = false;
            debugError('  %O: Patient data is not found', memberId);
          }
          if (_id !== undefined && !equal(chunk, patient)) {
            valid = false;
            debugError('  %O: Patient data does not match', memberId);
            debugError('     Diff: %O', diff(patient, chunk));
          }
          // End 1

          // 2. Patient IDs - where first name is missing
          if (!chunk['First Name']) {
            valid = false;
            debugError('  %O: Field %O is empty', memberId, 'First Name');
          }
          // End 2

          // 3. Patient IDs - Email address is missing but consent is Y
          if (!chunk['Email Address'] && chunk['CONSENT'] === 'Y') {
            valid = false;
            debugError(
              '  %O: Field %O is empty but CONSENT = %O',
              memberId,
              'Email Address',
              'Y'
            );
          }
          // End 3

          // 4. Verify Emails were created in Email Collection for patients who have
          // CONSENT as Y
          if (_id !== undefined && chunk['CONSENT'] === 'Y') {
            const emailsMongo = new Engine(
              emailsCollection,
              mongoDB,
              'emailID'
            );
            let validEmails = true;
            for (let index = 1; index < 5; index++) {
              const emails = await emailsMongo.findAll({
                'patient.Member ID': memberId,
                subject: `Day ${index}`,
              });
              if (emails.length !== 1) {
                validEmails = false;
                debugError(
                  '  %O: There is no %O email',
                  memberId,
                  `Day ${index}`
                );
              }
            }

            // 5. Verify the Email schedule matches with the above
            if (validEmails) {
              const emails = await emailsMongo.findAll(
                {
                  'patient.Member ID': memberId,
                },
                { sendDate: 1 }
              );
              validEmails = emails.reduce(
                (acc, email, index) =>
                  acc && email.subject === `Day ${index + 1}`,
                validEmails
              );
              if (!validEmails) {
                debugError('  %O: Emails are in wrong order', memberId);
              }
            }
            // End 5

            valid = validEmails;
            await emailsMongo.close();
          }
          // End 4

          if (valid) {
            debugValid('%O: Patient data is valid', memberId);
          }
        } catch (e) {
          debugError(e);
        }
      })
      .on('end', () => {
        debug('file stream finished!');
      });
  } catch (e) {
    debugError(e);
  }
}

/**
 * Import action function for commander
 *
 * @param opts
 * @returns {Promise<void>}
 */
async function importAction(opts) {
  try {
    const fileName = opts.file || '';
    debug('input file %O', fileName);
    await checkFileExistence(fileName);
    await csvFile2Mongo(fileName);
  } catch (e) {
    debugError(e);
  }
}

/**
 * Validate action function for commander
 *
 * @param opts
 * @returns {Promise<void>}
 */
async function validateAction(opts) {
  try {
    const fileName = opts.file || '';
    debug('input file %O', fileName);
    await checkFileExistence(fileName);
    await csvFileValidate(fileName);
  } catch (e) {
    debugError(e);
  }
}

/**
 * commander setup
 */
program.version('1.0.0', '-v, --version');

program
  .command('import')
  .description('execute import')
  .requiredOption('-f, --file <filename>', 'data source filename')
  .action(importAction);

program
  .command('validate', { isDefault: true })
  .description('validate data file')
  .requiredOption('-f, --file <filename>', 'data source filename')
  .action(validateAction);

program.parseAsync(process.argv).then((r) => null);
